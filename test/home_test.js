var chai = require('chai');
var chaiHttp = require('chai-http');
// Interesting part
var app = require('../bin/app');
//var loginUser = require('./login.js');
//var auth = {token: ''};

chai.use(chaiHttp);
chai.should();

var requester = chai.request(app);

describe('Home', function() {

  //beforeEach(function(done) {
    //loginUser(auth, done);
  //});

  it('returns status 200.', function(done) {
    // This is what launch the server
    requester
    .get('/')
    //.set('Authorization', auth.token)
    .then(function (res) {
      res.should.have.status(200);
      //res.should.be.json;
      //res.body.should.be.instanceof(Array).and.have.length(1);
      //res.body[0].should.have.property('username').equal('admin');
      done();
    })
    .catch(function (err) {
      return done(err);
    });
  });
})

